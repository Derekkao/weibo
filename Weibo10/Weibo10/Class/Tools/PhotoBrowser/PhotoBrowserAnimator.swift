//
//  PhotoBrowserAnimator.swift
//  Weibo10
//
//  Created by Derek on 2020/4/15.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

//MARK: - 展現動畫協議
protocol PhotoBrowserPresentDelegate: NSObjectProtocol {
    
    ///指定 indexPath 對應的 imageView 用來做動畫效果
    func imageViewForPresent(indexPath: IndexPath) -> UIImageView
    ///動畫轉場的起始位置
    func photoBrowserPresentFromRect(indexPath: IndexPath) -> CGRect
    ///動畫轉場的目標位置
    func photoBrowserPresentToRect(indexPath: IndexPath) -> CGRect
}

//MARK: - 解除動畫協議
protocol PhotoBrowserDismissDelegate: NSObjectProtocol {
    
    ///解除轉場的圖像視圖（包含起始位置）
    func imageViewForDismiss() -> UIImageView
    ///解除轉場的圖像索引
    func indexPathForDismiss() -> IndexPath
}

//MARK: - 提供動畫轉場代理
class PhotoBrowserAnimator: NSObject, UIViewControllerTransitioningDelegate {
    
    weak var presentDelegate: PhotoBrowserPresentDelegate?
    weak var dismissDelegate: PhotoBrowserDismissDelegate?
    ///動畫圖像的索引
    var indexPath: IndexPath?
    
    ///是否 modal 展現的標記
    private var isPresented = false
    
    ///設置代理相關參數
    func setDelegateParams(presentDelegate: PhotoBrowserPresentDelegate, indexPath: IndexPath, dismissDelegate: PhotoBrowserDismissDelegate) {
        self.presentDelegate = presentDelegate
        self.indexPath = indexPath
        self.dismissDelegate = dismissDelegate
    }
    
    //返回提供 modal 展現的動畫的對象
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresented = true
        return self
    }
    
    //返回提供 dismiss 的動畫對象
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresented = false
        return self
    }
}

//MARK: - UIViewControllerAnimatedTransitioning
//實現具體的動畫方法
extension PhotoBrowserAnimator: UIViewControllerAnimatedTransitioning {
    
    //動畫時長
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    //實現具體的動畫效果 - 一旦實現了此方法，所有的動畫都交由程序員負責
    //transitionContext: 轉場動畫的上下文 - 提供動畫所需要的素材
    /*
        1. 容器視圖 - 會將 modal 要展現的視圖包裝在容器視圖中
           存放的視圖要顯示 - 必須自己指定大小，不會通過自動佈局填滿屏幕
        2. viewControllerForKey: fromVC / toVC
        3. viewForKey: toView
        4. completeTransition: 無論轉場是否被取消，都必須調用
           否則，系統不做其他事件處理
     */
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        //自動佈局系統不會對根視圖做任何約束
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        print(fromVC)
        
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        print(toVC)
        
        let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)
        let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)
        
        isPresented ? presentAnimation(transitionContext: transitionContext) : dismissAnimation(transitionContext: transitionContext)
        
    }
    
    ///解除轉場動畫
    private func dismissAnimation(transitionContext: UIViewControllerContextTransitioning) {
        
        // guard let 會把屬性變成局部變量，後續閉包中不需要 self，也不需要考慮解包
        guard let presentDelegate = presentDelegate, let dismissDelegate = dismissDelegate else {
            return
        }
        //獲取要 dismiss 的控制器視圖
        let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)
        fromView?.removeFromSuperview()
        //獲取圖像視圖
        let iv = dismissDelegate.imageViewForDismiss()
        //添加到容器視圖
        transitionContext.containerView.addSubview(iv)
        //獲取 dismiss 的 indexPath
        let indexPath = dismissDelegate.indexPathForDismiss()
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            //讓 iv 運動到目標位置
            iv.frame = presentDelegate.photoBrowserPresentFromRect(indexPath: indexPath)
        }) { (_) in
            //將 iv 從父視圖中刪除
            iv.removeFromSuperview()
            //告訴系統轉場動畫完成
            transitionContext.completeTransition(true)
        }
    }
    
    ///展現動畫
    private func presentAnimation(transitionContext: UIViewControllerContextTransitioning) {
        
        guard let presentDelegate = presentDelegate, let indexPath = indexPath else {
            return
        }
        //目標視圖
        //1. 獲取 modal 要展現的控制器的根視圖
        let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)
        //2. 將視圖添加到容器視圖中
        transitionContext.containerView.addSubview(toView!)
        
        //能夠拿到參與動畫的圖像視圖/起始位置/目標位置
        let iv = presentDelegate.imageViewForPresent(indexPath: indexPath)
        //指定圖像視圖位置
        iv.frame = presentDelegate.photoBrowserPresentFromRect(indexPath: indexPath)
        //將圖像視圖添加到容器視圖
        transitionContext.containerView.addSubview(iv)
        
        toView?.alpha = 0
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            iv.frame = presentDelegate.photoBrowserPresentToRect(indexPath: indexPath)
        }) { (_) in
            toView?.alpha = 1
            //將圖像視圖刪除
            iv.removeFromSuperview()
            //告訴系統轉場動畫完成
            transitionContext.completeTransition(true)
        }
    }
}
