//
//  PhotoBrowserCell.swift
//  Weibo10
//
//  Created by Derek on 2020/4/14.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import SDWebImage

protocol PhotoBrowserCellDelegate: NSObjectProtocol {
    func photoBrowserCellDidTapImage()
}

///照片查看 cell
class PhotoBrowserCell: UICollectionViewCell {
    
    weak var photoDelegate: PhotoBrowserCellDelegate?
    
    //MARK: - 監聽方法
    @objc private func tapImage() {
        photoDelegate?.photoBrowserCellDidTapImage()
    }
    
    /*
        手勢識別是對 touch 的一個封裝，UIScrollView 支持捏合手勢，一般做過手勢監聽的控件，都會屏蔽掉 touch 事件
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    //MARK - 圖像地址
    var imageURL: URL? {
        didSet {
            guard let url = imageURL else {
                return
            }
            
            resetScrollView()
            
            //因為中間會加載，所以先設定縮圖
            //1> 從磁盤加載縮圖
            let placeholderImage = SDImageCache.shared.imageFromDiskCache(forKey: url.absoluteString)
            setPlaceHolder(image: placeholderImage)
            
            // SDWebImage 有一個功能，一旦設置了 url，準備異步加載
            // 清除之前的圖片/如果之前的圖片也是異步下載，但是沒有完成，就取消之前的異步操作
            // 如果 url 對應的圖像已經被緩存，直接從磁盤讀取，不會再走網路加載
            imageView.sd_setImage(with: bmiddleURL(url: url), placeholderImage: nil, options: [.refreshCached, .retryFailed], progress: { (current, total, _) in
                
                DispatchQueue.main.async {
                    self.placeHolder.progress = CGFloat(current) / CGFloat(total)
                }
            }) { (image, _, _, _) in
                
                //判斷圖像下載是否成功
                if image == nil {
                    self.contentView.makeToast("您的網路不給力")
                    return
                }
                
                //隱藏佔位圖像
                self.placeHolder.isHidden = true
                //設置圖像視圖位置
                self.setPosition(image: image ?? UIImage())
            }
        }
    }
    
    ///設置佔位視圖的內容
     //image: 本地緩存的縮圖，如果縮圖下載失敗，image 為 nil
    private func setPlaceHolder(image: UIImage?) {
        placeHolder.isHidden = false
        placeHolder.image = image
        //2> 設置大小
        placeHolder.sizeToFit()
        //3> 設置中心點
        placeHolder.center = scrollView.center
    }
    
    ///重設 scrollView 內容屬性
    private func resetScrollView() {
        //重設 imageView 的內容屬性 - scrollView 在處理縮放的時候，是調整代理方法返回視圖的 transform 來實現的
        imageView.transform = .identity
        //重設 scrollView 的內容屬性
        scrollView.contentInset = .zero
        scrollView.contentOffset = .zero
        scrollView.contentSize = .zero
    }
    
    ///設置 imageView 的位置
    private func setPosition(image: UIImage) {
        //自動設置大小
        let size = self.displaySize(image: image)
        //判斷圖片高度
        if size.height < scrollView.bounds.height {
            //上下置中顯示 - 調整 frame 的 x/y，一旦縮放，影響滾動範圍
            let y = (scrollView.bounds.height - size.height) * 0.5
            imageView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            
            //內容邊距 - 會調整控件位置，但是不會影響控件的滾動
            scrollView.contentInset = UIEdgeInsets(top: y, left: 0, bottom: 0, right: 0)
        } else {
            //長圖 (要可以滾動)
            self.imageView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height - 50)
            scrollView.contentSize = size
        }
    }
    
    ///根據 scrollView 的寬度計算等比例縮放之後的圖片尺寸
    private func displaySize(image: UIImage) -> CGSize {
        let w = scrollView.bounds.width
        let h = image.size.height * w / image.size.width
        return CGSize(width: w, height: h)
    }
    
    private func bmiddleURL(url: URL) -> URL {
        var urlString = url.absoluteString
        urlString = urlString.replacingOccurrences(of: "/thumbnail/", with: "/bmiddle/")
        return URL(string: urlString)!
    }
    
    //MARK: - 構造函數
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        contentView.addSubview(scrollView)
        scrollView.addSubview(imageView)
        scrollView.addSubview(placeHolder)
        
        var rect = bounds
        rect.size.width -= 20
        scrollView.frame = rect
        
        //設置 scrollView 縮放
        scrollView.delegate = self
        scrollView.minimumZoomScale = 0.5
        scrollView.maximumZoomScale = 2.0
        
        //添加手勢識別
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapImage))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tap)
    }
    
    //MARK: - 懶加載控件
    lazy var scrollView = UIScrollView()
    lazy var imageView = UIImageView()
    private lazy var placeHolder = ProgressImageView()
}

//MARK: - UIScrollViewDelegate
extension PhotoBrowserCell: UIScrollViewDelegate {
    
    ///返回被縮放的視圖
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    ///縮放完成後執行一次
    /*
        view: view 被縮放的視圖
        scale: 被縮放的比例
     */
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        var offsetY = (scrollView.bounds.height - (view?.frame.height)!) * 0.5
        offsetY = offsetY < 0 ? 0 : offsetY
        var offsetX = (scrollView.bounds.width - (view?.frame.width)!) * 0.5
        offsetX = offsetX < 0 ? 0 : offsetX
        //設置間距
        scrollView.contentInset = UIEdgeInsets(top: offsetY, left: offsetX, bottom: 0, right: 0)
    }
    
    ///只要縮放就會被調用
    /*
        a d => 縮放比例
        a b c d => 共同決定旋轉
        tx ty => 設置位移
        定義控件位置 frame = center + bounds * transform
     */
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
    }
}
