//
//  UserAccountViewModel.swift
//  Weibo10
//
//  Created by Derek on 2020/3/31.
//  Copyright © 2020 Derek. All rights reserved.
//

import Foundation

///用戶帳號視圖模型 - 沒有父類
/*
   模型通常繼承自 NSObject -> 可以使用 KVC 設置屬性，簡化對象構造
   如果沒有父類，所有的內容，都需要從頭創建，量級更輕
   視圖模型的作用：封裝業務邏輯，通常沒有複雜的屬性
 */
class UserAccountViewModel {
    
    ///單例 - 解決避免重複從沙盒加載，歸檔文件，提高效率，讓 access_token 便於被訪問到
    static let sharedUserAccount = UserAccountViewModel()
    
    ///用戶模型
    var account: UserAccount?
    
    ///返回有效的 token
    var accessToken: String? {
        if !isExpired {
            return account?.access_token
        }
        return nil
    }
    
    ///用戶登錄標記
    var userLogon: Bool {
        //1. 如果 token 有值，說明登錄成功
        //2. 如果沒有過期，說明登錄有效
        return account?.access_token != nil && !isExpired 
    }
    
    ///用戶頭像 URL
    var avatarUrl: URL {
        return URL(string: account?.avatar_large ?? "")!
    }
    
    ///歸檔保存的路徑 - 計算型屬性(類似於有返回值的函數)
    private var accountPath: URL {
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!.appendingPathComponent("account.plist")
        return path
    }
    
    ///判斷帳戶是否過期
    private var isExpired: Bool {
        //判斷用戶帳戶過期日期與當前系統日期進行比較
        //自己改寫日期，可以測試邏輯是否正確，創建日期的時候，如給定負數，返回比當前時間早的日期
        //account?.expireDate = Date(timeIntervalSinceNow: -3600)
        //如果 account 為 nil，不會調用後面的屬性，後面的 compare 也不會繼續...
        if account?.expireDate?.compare(Date()) == ComparisonResult.orderedDescending {
            return false
        }
        //如果過期返回 true
        return true
    }
    
    ///構造函數 - 私有化，會要求外部只能通過單例常量訪問，而不能 () 實例化
    private init() {
        //從沙盒解檔數據，恢復當前數據
        loadUserAccount()
        
        //判斷 token 是否過期
        if isExpired {
            print("已經過期")
            //如果過期，清空解檔的數據
            account = nil
        }
    }
}

//MARK: - 用戶帳戶相關的網路方法
/*
    代碼重構的步驟
    1. 新方法
    2. 貼上代碼
    3. 根據上下文調整參數和返回值
    4. 移動其他子方法
 */
extension UserAccountViewModel {
    func loadAccessToken(code: String, finished:@escaping (_ isSuccess: Bool) -> Void) {
        //加載 accessToken
        NetworkTools.sharedTools.loadAccessToken(code: code) { (result, error) in
            
            // 1. 判斷錯誤
            if error != nil {
                print("出錯了")
                finished(false)
                return
            }
            
            // 2. 輸出結果
            self.account = UserAccount(dict: result as? [String: Any] ?? [String: Any]())
            self.loadUserInfo(account: self.account!, finished: finished)
        }
    }
    
    ///加載用戶信息
    private func loadUserInfo(account: UserAccount, finished:@escaping (_ isSuccess: Bool) -> Void) {
        NetworkTools.sharedTools.loadUserInfo(uid: account.uid ?? "") { (result, error) in
            if error != nil {
                print("加載用戶出錯了")
                finished(false)
                return
            }
            
            guard let dict = result as? [String: Any] else {
                print("格式錯誤")
                finished(false)
                return
            }
            
            //將用戶信息保存
            account.screen_name = dict["screen_name"] as? String
            account.avatar_large = dict["avatar_large"] as? String
            
            print(account.screen_name ?? "")
            print(account.avatar_large ?? "")
            
            //保存對象
            self.saveUserAccount()
            
            //需要完成回調!!!
            finished(true)
        }
    }
}

//MARK: - 歸檔和解檔
extension UserAccountViewModel {

    ///保存當前對象
    func saveUserAccount() {
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: account, requiringSecureCoding: false)
            try data.write(to: accountPath)
            print(self.accountPath)
        } catch {
            print("error is: \(error.localizedDescription)")
        }
    }
    
    ///讀取當前對象
    func loadUserAccount() {
        do {
            let data = try Data(contentsOf: accountPath)
            account = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? UserAccount
        } catch {
            print("error is: \(error.localizedDescription)")
        }
    }
}
