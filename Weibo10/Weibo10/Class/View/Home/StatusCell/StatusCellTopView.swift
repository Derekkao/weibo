//
//  StatusCellTopView.swift
//  Weibo10
//
//  Created by Derek on 2020/4/2.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

///頂部視圖
class StatusCellTopView: UIView {
    
    ///微博視圖模型
    var viewModel: StatusViewModel? {
        didSet {
            //設置工作
            self.iconView.sd_setImage(with: viewModel?.userProfileUrl, placeholderImage: viewModel?.userDefaultIconView, options: [])
            self.nameLabel.text = viewModel?.status.user?.screen_name
            self.vipIconView.image = viewModel?.userVipImage
            //MARK: TODO - 後續改
            self.timeLabel.text = "剛剛" //viewModel?.status.created_at
            self.sourceLabel.text = "來自黑馬微博"  //viewModel?.status.source
        }
    }
    
    //MARK: - 構造函數
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - 懶加載控件
    ///頭像
    private lazy var iconView = UIImageView(image: #imageLiteral(resourceName: "user"))
    ///姓名
    private lazy var nameLabel = UILabel(title: "王老五", fontSize: 14)
    //FIXME: - 有可能已經沒有參數對應
    private lazy var memberIconView = UIImageView(image: #imageLiteral(resourceName: "user"))
    ///認證圖標
    private lazy var vipIconView = UIImageView(image: #imageLiteral(resourceName: "medal"))
    ///時間標籤
    private lazy var timeLabel = UILabel(title: "現在", fontSize: 11, color: .orange)
    ///來源標籤
    private lazy var sourceLabel = UILabel(title: "來源", fontSize: 11)
}

//MARK: - 設置介面
extension StatusCellTopView {
    private func setupUI() {
        
        //添加分隔視圖
        let sepView = UIView()
        sepView.backgroundColor = .lightGray
        addSubview(sepView)
        
        addSubview(iconView)
        addSubview(nameLabel)
        addSubview(vipIconView)
        addSubview(timeLabel)
        addSubview(sourceLabel)
        
        sepView.snp_makeConstraints { (make) in
            make.top.equalTo(snp_top)
            make.left.equalTo(snp_left)
            make.right.equalTo(snp_right)
            make.height.equalTo(StatusCellMargin)
        }
        
        iconView.snp_makeConstraints { (make) in
            make.top.equalTo(sepView.snp_bottom).offset(StatusCellMargin)
            make.left.equalTo(snp_left).offset(StatusCellMargin)
            make.width.equalTo(StatusCellIconWidth)
            make.height.equalTo(StatusCellIconWidth)
        }
        nameLabel.snp_makeConstraints { (make) in
            make.top.equalTo(iconView.snp_top)
            make.left.equalTo(iconView.snp_right).offset(StatusCellMargin)
        }
        vipIconView.snp_makeConstraints { (make) in
            make.centerX.equalTo(iconView.snp_right)
            make.centerY.equalTo(iconView.snp_bottom)
            make.width.equalTo(StatusCellVipIconWidth)
            make.height.equalTo(StatusCellVipIconWidth)
        }
        timeLabel.snp_makeConstraints { (make) in
            make.bottom.equalTo(iconView.snp_bottom)
            make.left.equalTo(iconView.snp_right).offset(StatusCellMargin)
        }
        sourceLabel.snp_makeConstraints { (make) in
            make.bottom.equalTo(iconView.snp_bottom)
            make.left.equalTo(timeLabel.snp_right).offset(StatusCellMargin)
        }
    }
}
