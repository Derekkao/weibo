//
//  ComposeViewController.swift
//  Weibo10
//
//  Created by Derek on 2020/4/9.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

//MARK: - 撰寫控制器
class ComposeViewController: UIViewController {

    ///照片選擇控制器
    private var picturePickerController = PicturePickerViewController()
    
    ///判斷 textView 是否有字
    var hasText: Bool {
        return textView.text == "" ? false : true
    }
    
    //MARK: - 監聽方法
    @objc private func close() {
        dismiss(animated: true, completion: nil)
    }
    
    ///發布微博
    @objc private func sendStatus() {
        print("發布微博")
        //1. 獲得文本內容
        let text = textView.text
        
        //2. 發布微博
        //MARK: - 微博已經修改新的 API，導致無法 post 成功
        let image = picturePickerController.pictures.last
        NetworkTools.sharedTools.sendStatus(status: text ?? "", image: image) { (result, error) in
            if error != nil {
                print("出錯了")
                self.view.makeToast("您的網路不給力")
                return
            }
            print(result)
            
            self.close()
        }
    }
    
    ///選擇照片
    @objc private func selectPicture() {
        print("選擇照片")
        
        textView.resignFirstResponder()
        
        //0. 為了避免一直重複更新，判斷如果已經更新了約束，不再執行後續代碼
        if picturePickerController.view.frame.height > 0 {
            return
        }
        
        //1. 修改照片選擇控制器視圖的約束
        picturePickerController.view.snp_updateConstraints { (make) in
            make.height.equalTo(view.bounds.height * 0.6)
        }
        //2. 修改文本視圖的約束 - 重建約束 - 會將之前 textView 的所有約束刪除
        textView.snp_remakeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(view.snp_left)
            make.right.equalTo(view.snp_right)
            make.bottom.equalTo(picturePickerController.view.snp_top)
        }
        //3. 動畫更新約束
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    ///選擇表情
    @objc private func selectEmoticon() {
        print("選擇表情")
    }
    
    //MARK: - 鍵盤處理
    @objc private func keyboardChanged(notify: Notification) {
        
        //1. 獲取目標的 rect - 字典中的結構體是 NSValue
        let rect = (notify.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        //print(rect)
        
        //獲取目標動畫時長 - 字典中的數值是 NSNumber
        let duration = (notify.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let offset = -UIScreen.main.bounds.height + rect.origin.y
        
        //2. 更新約束
        toolbar.snp_updateConstraints { (make) in
            make.bottom.equalTo(view.snp_bottom).offset(offset)
        }
        
        //3. 動畫
        UIView.animate(withDuration: duration) {
            self.view.layoutIfNeeded()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.delegate = self
        //添加鍵盤通知
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardChanged(notify:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - 視圖生命週期
    override func loadView() {
        view = UIView()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //如果已經存在照片控制器視圖，不再激活鍵盤
        if picturePickerController.view.frame.height == 0 {
            textView.becomeFirstResponder()
        }
    }
    
    //MARK: - 懶加載控件
    ///工具條
    private lazy var toolbar = UIToolbar()
    private lazy var textView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 18)
        tv.textColor = .darkGray
        tv.alwaysBounceVertical = true
        tv.keyboardDismissMode = .onDrag
        return tv
    }()
    ///佔位標籤
    private lazy var placeHolderLabel = UILabel(title: "分享新鮮事...", fontSize: 18, color: .lightGray)
}

//MARK: - UITextViewDelegate
extension ComposeViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        navigationItem.rightBarButtonItem?.isEnabled = hasText
        placeHolderLabel.isHidden = hasText
    }
}

//MARK: - 設置介面
private extension ComposeViewController {
    func setupUI() {
        
        //取消自動調整滾動視圖的間距
        automaticallyAdjustsScrollViewInsets = false
        
        view.backgroundColor = .white
        prepareNavigationBar()
        prepareToolbar()
        prepareTextView()
        preparePicturePicker()
    }
    
    private func preparePicturePicker() {
        addChild(picturePickerController)
        view.insertSubview(picturePickerController.view, belowSubview: toolbar)
        picturePickerController.view.snp_makeConstraints { (make) in
            make.bottom.equalTo(view.snp_bottom)
            make.left.equalTo(view.snp_left)
            make.right.equalTo(view.snp_right)
            make.height.equalTo(0)
        }
    }
    
    ///準備工具條
    func prepareToolbar() {
        view.addSubview(toolbar)
        toolbar.snp_makeConstraints { (make) in
            make.bottom.equalTo(view.snp_bottom)
            make.left.equalTo(view.snp_left)
            make.right.equalTo(view.snp_right)
            make.height.equalTo(44)
        }
        
        let itemSettings = [
            ["imageName":"imagebackground", "actionName":"selectPicture"],
            ["imageName":"at"],
            ["imageName":"number"],
            ["imageName":"emoji", "actionName":"selectEmoticon"],
            ["imageName":"more"]
        ]
        var items = [UIBarButtonItem]()
        
        for dict in itemSettings {
            items.append(UIBarButtonItem(imageName: dict["imageName"]!, target: self, actionName: dict["actionName"]))
            items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil))
        }
        items.removeLast()
        toolbar.items = items
    }
    
    ///準備文本視圖
    func prepareTextView() {
        view.addSubview(textView)
        textView.snp_makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(view.snp_left)
            make.right.equalTo(view.snp_right)
            make.bottom.equalTo(toolbar.snp_top)
        }
        
        textView.addSubview(placeHolderLabel)
        placeHolderLabel.snp_makeConstraints { (make) in
            make.top.equalTo(textView.snp_top).offset(8)
            make.left.equalTo(textView.snp_left).offset(5)
        }
    }
    
    ///設置導航欄
    func prepareNavigationBar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "取消", style: .plain, target: self, action: #selector(close))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "發布", style: .plain, target: self, action: #selector(sendStatus))
        navigationItem.rightBarButtonItem?.isEnabled = false
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 32))
        navigationItem.titleView = titleView
        let titleLabel = UILabel(title: "發微博", fontSize: 15)
        let nameLabel = UILabel(title: UserAccountViewModel.sharedUserAccount.account?.screen_name ?? "", fontSize: 13, color: .lightGray)
        titleView.addSubview(titleLabel)
        titleView.addSubview(nameLabel)
        titleLabel.snp_makeConstraints { (make) in
            make.centerX.equalTo(titleView.snp_centerX)
            make.top.equalTo(titleView.snp_top)
        }
        nameLabel.snp_makeConstraints { (make) in
            make.centerX.equalTo(titleView.snp_centerX)
            make.bottom.equalTo(titleView.snp_bottom)
        }
    }
}
