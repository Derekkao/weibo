//
//  User.swift
//  Weibo10
//
//  Created by Derek on 2020/4/1.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

///用戶模型
class User: NSObject {
    
    ///用戶UID
    var id: Int = 0
    ///用戶暱稱
    var screen_name: String?
    ///用戶頭像地址，50x50像素
    var profile_image_url: String?
    ///認證類型：'-1 = 沒有認證' '0 = 認證用戶' '2、3、5 = 企業認證' '220 = 達人'
    var verified_type: Int = 0
    
    init(dict: [String: Any]) {
        self.id = dict["id"] as? Int ?? 0
        self.screen_name = dict["screen_name"] as? String
        self.profile_image_url = dict["profile_image_url"] as? String
        self.verified_type = dict["verified_type"] as? Int ?? 0
    }
}
