//
//  HomeTableViewController.swift
//  Weibo10
//
//  Created by Derek on 2020/3/28.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

///原創微博 cell 的可重用 id
let StatusCellNormalId = "StatusCellNormalId"
///轉發微博 cell 的可重用 id
let StatusCellRetweetedId = "StatusCellRetweetedId"

class HomeTableViewController: VisitorTableViewController {
    
    ///微博數據列表模型
    private lazy var listViewModel = StatusListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !UserAccountViewModel.sharedUserAccount.userLogon {
            visitorView?.setupInfo(imageName: nil, title: "關注一些人，回這裡看看有什麼驚喜")
            return
        }
        print("token:", UserAccountViewModel.sharedUserAccount.accessToken ?? "")
        prepareTableView()
        loadData()
        
        //註冊通知 - 如果使用通知中心的 block 監聽，其中的 self 一定要弱引用，不然 deinit 也不會釋放
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: WBStatusSelectedPhotoNotification), object: nil, queue: nil) { [weak self](notify) in
            
            guard let indexPath = notify.userInfo?[WBStatusSelectedPhotoIndexPathKey] as? IndexPath else {
                return
            }
            guard let urls = notify.userInfo?[WBStatusSelectedPhotoURLsKey] as? [URL] else {
                return
            }
            //判斷 cell 是否遵守了展現動畫協議
            guard let cell = notify.object as? PhotoBrowserPresentDelegate else {
                return
            }
            
            let vc = PhotoBrowserViewController(urls: urls, indexPath: indexPath)
            //設置 modal 的類型是自定義類型
            vc.modalPresentationStyle = .custom
            //設置動畫代理
            vc.transitioningDelegate = self?.photoBrowserAnimator
            //設置 animator 的代理參數
            self?.photoBrowserAnimator.setDelegateParams(presentDelegate: cell, indexPath: indexPath, dismissDelegate: vc)
            self?.present(vc, animated: true, completion: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    ///準備表格
    private func prepareTableView() {
        tableView.register(StatusNormalCell.self, forCellReuseIdentifier: StatusCellNormalId)
        tableView.register(StatusRetweetedCell.self, forCellReuseIdentifier: StatusCellRetweetedId)
        //預估行高 - 需要盡量準確
        tableView.estimatedRowHeight = 400
        //取消分隔線
        tableView.separatorStyle = .none
        //下拉刷新控件默認沒有
        refreshControl = UIRefreshControl()
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
        refreshControl?.addSubview(view)
        //添加監聽方法
        refreshControl?.addTarget(self, action: #selector(loadData), for: .valueChanged)
        //refreshControl?.tintColor = .clear
        
        //上拉刷新視圖
        tableView.tableFooterView = pullupView
    }
    
    ///加載數據
    @objc private func loadData() {
        
        listViewModel.loadStatus(isPullup: pullupView.isAnimating) { (isSuccess) in

            //關閉刷新控件
            self.refreshControl?.endRefreshing()
            //關閉上拉刷新
            self.pullupView.stopAnimating()
            
            if !isSuccess {
                self.view.makeToast("加載數據錯誤，請稍後再試！")
                return
            }
            print(self.listViewModel.statusList[1].status.user?.screen_name ?? "")
            
            //測試用
            for statusDict in self.listViewModel.statusList {
                //print(statusDict.status.retweeted_status)
            }
            //刷新數據
            self.tableView.reloadData()
        }
    }
    
    //MARK: - 懶加載控件
    //上拉刷新提示視圖
    private lazy var pullupView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .whiteLarge)
        indicator.color = .lightGray
        return indicator
    }()
    
    ///照片查看轉場動畫代理
    private lazy var photoBrowserAnimator = PhotoBrowserAnimator()
}

//MARK: - 數據源方法
extension HomeTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listViewModel.statusList.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let vm = listViewModel.statusList[indexPath.row]
        
        //不會調用行高方法
        //tableView.dequeueReusableCell(withIdentifier:)
        //會調用行高方法
        let cell = tableView.dequeueReusableCell(withIdentifier: vm.cellId, for: indexPath) as! StatusCell  //不需要改成 StatusRetweetedCell，因為多態
        cell.viewModel = vm
        
        //判斷是否是最後一條微博
        if indexPath.row == listViewModel.statusList.count - 1 && !pullupView.isAnimating {
            //開始動畫
            pullupView.startAnimating()
            //上拉刷新數據
            loadData()
            print("上拉刷新數據")
        }
        return cell
    }
    
    /*
        行高
        -- 設置了預估行高 estimatedRowHeight
        * 當前顯示的行高法會調用三次 (每個版本的 Xcode 調用次數可能不同)
     
        問題：預估行高如果不同，計算的次數不同
        1. 使用預估行高，計算出預估的 contentSize
        2. 根據預估行高，判斷計算次數，順序計算每一行的行高，更新 contentSize
        3. 如果預估行高過大，超出預估範圍，順序計算後續行高，一直到填滿屏幕退出，同時更新 contentSize
        4. 使用預估行高，每個 cell 的顯示前需要計算，單個 cell 的效率是低的，從整體效率高！
     
        執行順序：行數 -> 每個 cell 都會調用 -> 行高
        預估行高：盡量靠近！
        
     
        -- 沒設置預估行高 estimatedRowHeight
        * 1. 計算所有行的高度
        * 2. 再計算顯示行的高度
     
        執行順序：行數 -> 行高 -> cell
     
        為什麼要調用所有行高方法？
        UITableView 繼承自 UIScrollView
        表格視圖滾動非常流暢 -> 需要提前計算出 contentSize
        
        如果行高是固定值，就不要實現行高代理方法，不然會一直呼叫
     
        實際開發中，行高一定要緩存
     */
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return listViewModel.statusList[indexPath.row].rowHeight
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("選中行", indexPath)
    }
}
