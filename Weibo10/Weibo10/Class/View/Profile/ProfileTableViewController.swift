//
//  ProfileTableViewController.swift
//  Weibo10
//
//  Created by Derek on 2020/3/28.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

class ProfileTableViewController: VisitorTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        visitorView?.setupInfo(imageName: "user", title: "登錄後，你的微博、相冊、個人資料會顯示在這裡，展示給別人")
    }
}
