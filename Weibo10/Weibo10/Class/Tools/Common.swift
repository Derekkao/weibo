//
//  Common.swift
//  Weibo10
//
//  Created by Derek on 2020/3/30.
//  Copyright © 2020 Derek. All rights reserved.
//

//提供全局共享屬性或方法
import UIKit

let WBAppearanceTintColor = UIColor.orange

///切換根視圖控制器通知
let WBSwitchRootViewControllerNotification = "WBSwitchRootViewControllerNotification"
///選中照片通知
let WBStatusSelectedPhotoNotification = "WBStatusSelectedPhotoNotification"

///選中照片的 KEY - indexPath
let WBStatusSelectedPhotoIndexPathKey = "WBStatusSelectedPhotoIndexPathKey"
///選中照片的 KEY - URL 數組
let WBStatusSelectedPhotoURLsKey = "WBStatusSelectedPhotoURLsKey"
