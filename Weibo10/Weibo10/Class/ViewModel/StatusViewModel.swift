//
//  StatusViewModel.swift
//  Weibo10
//
//  Created by Derek on 2020/4/2.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

///微博視圖模型 - 處理單條微博的業務邏輯
class StatusViewModel {
    
    ///微博的模型
    var status: Status
    
    ///表格的可重用標示符號
    var cellId: String {
        return status.retweeted_status != nil ? StatusCellRetweetedId : StatusCellNormalId
    }
    
    ///緩存的行高 - 懶加載特性：沒有值就去計算，有值就直接拿之前算過的
    lazy var rowHeight: CGFloat = {
        
        //1. cell
        var cell: StatusCell
        //2. 根據是否是轉發微博，決定 cell 的創建
        if self.status.retweeted_status != nil {
            //因為創建對象，所以不能用原本的 StatusCell，要用 StatusRetweetedCell
            cell = StatusRetweetedCell(style: .default, reuseIdentifier: StatusCellRetweetedId)
        } else {
            cell = StatusNormalCell(style: .default, reuseIdentifier: StatusCellNormalId)
        }
        
        //2. 紀錄高度
        return cell.rowHeight(vm: self)
    }()
    
    ///用戶頭像 URL
    var userProfileUrl: URL {
        return URL(string: status.user?.profile_image_url ?? "")!
    }
    
    ///用戶默認頭像
    var userDefaultIconView: UIImage {
        return #imageLiteral(resourceName: "user")
    }
    
    ///用戶
    var userVipImage: UIImage? {
        switch status.user?.verified_type {
        case -1:
            return #imageLiteral(resourceName: "new")
        case 0:
            return #imageLiteral(resourceName: "medal")
        case 2,3,5:
            return #imageLiteral(resourceName: "business")
        case 220:
            return #imageLiteral(resourceName: "premium")
        default:
            return nil
        }
    }
    
    ///縮圖 URL 數組 - 存儲型屬性 (不使用計算屬性原因：不讓他每次計算，造成效率太慢)
    ///如果是原創微博，可以有圖，可以沒有圖
    ///如果是轉發微博，一定沒有圖，retweeted_status 中，可以有圖，也可以沒有圖
    ///一條微博，最多只有一個 pic_urls 數組
    var thumbnailUrls: [URL]?
    
    var retweetedText: String? {
        
        //1. 判斷是否是轉發微博，如果不是直接返回 nil
        guard let s = status.retweeted_status else {
            return nil
        }
        //2. s 就是轉發微博
        return "@\(s.user?.screen_name ?? ""):\(s.text ?? "")"
    }
    
    ///構造函數
    init(status: Status) {
        self.status = status
        
        //根據模型，生成縮圖的數組
        if let urls = status.retweeted_status?.pic_urls ?? status.pic_urls {
            
            //創建縮圖數組
            thumbnailUrls = [URL]()
            
            //遍歷字典數組
            for dict in urls {
                //因為字典是按照 key 來取值，如果 key 錯誤會返回 nil
                let url = URL(string: dict["thumbnail_pic"]!)
                //相信服務器返回的 url 字串一定可以生成
                thumbnailUrls?.append(url!)
            }
        }
    }
}
