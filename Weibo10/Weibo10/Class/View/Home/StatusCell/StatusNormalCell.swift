//
//  StatusNormalCell.swift
//  Weibo10
//
//  Created by Derek on 2020/4/5.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

///原創微博 cell
class StatusNormalCell: StatusCell {

    ///微博視圖模型
    override var viewModel: StatusViewModel? {
        didSet {
            pictureView.snp_updateConstraints { (make) in
                //根據配圖數量，決定配圖視圖的頂部間距
                let offset = viewModel?.thumbnailUrls?.count ?? 0 > 0 ? StatusCellMargin : 0
                make.top.equalTo(contentLabel.snp_bottom).offset(offset)
            }
        }
    }
    
    override func setupUI() {
        super.setupUI()
        pictureView.snp_makeConstraints { (make) in
            make.top.equalTo(contentLabel.snp_bottom).offset(StatusCellMargin)
            make.left.equalTo(contentLabel.snp_left)
            make.width.equalTo(300)
            make.height.equalTo(90)
        }
        
    }
}
