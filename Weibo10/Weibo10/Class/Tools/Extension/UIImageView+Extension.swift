//
//  UIImageView+Extension.swift
//  Weibo10
//
//  Created by Derek on 2020/3/30.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

extension UIImageView {
    
    ///便利構造函數
    convenience init(image: UIImage, contentMode: ContentMode = .scaleAspectFit) {
        self.init()
        self.image = image
        self.contentMode = contentMode
    }
}
