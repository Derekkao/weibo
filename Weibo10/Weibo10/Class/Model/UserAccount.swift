//
//  UserAccount.swift
//  Weibo10
//
//  Created by Derek on 2020/3/30.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

///用戶帳戶模型
class UserAccount: NSObject {
    ///用户授权的唯一票据，用于调用微博的开放接口，同时也是第三方应用验证微博用户登录的唯一票据，第三方应用应该用该票据和自己应用内的用户建立唯一影射关系，来识别登录状态，不能使用本返回值里的UID字段来做登录识别。
    var access_token: String?
    ///access_token的生命周期，单位是秒数。
    var expires_in: Double = 0
    ///授权用户的UID，本字段只是为了方便开发者，减少一次user/show接口调用而返回的，第三方应用不能用此字段作为用户登录状态的识别，只有access_token才是用户授权的唯一票据。
    var uid: String?
    var expireDate: Date?
    //用户昵称
    var screen_name: String?
    //用户头像地址（大图），180×180像素
    var avatar_large: String?
    
    init(dict: [String: Any]) {
        self.access_token = dict["access_token"] as? String
        self.expires_in = dict["expires_in"] as! Double
        self.uid = dict["uid"] as? String
        self.expireDate = Date(timeIntervalSinceNow: self.expires_in)
    }
    
    //MARK: - '鍵值'歸檔和解檔
    //編碼器
    //歸檔 - 把當前對象保存到磁盤前，將對象編碼成二進制數據 - 類似網路的序列化
    func encode(with coder: NSCoder) {
        coder.encode(access_token, forKey: "access_token")
        coder.encode(expireDate, forKey: "expireDate")
        coder.encode(uid, forKey: "uid")
        coder.encode(screen_name, forKey: "screen_name")
        coder.encode(avatar_large, forKey: "avatar_large")
    }
    
    //解碼器
    //解檔 - 從磁盤加載二進制文件，轉換成對象時調用 - 類似網路的反序列化
    // required - 沒有繼承性，所有的對象只能解檔出當前的類對象
    required init?(coder: NSCoder) {
        access_token = coder.decodeObject(forKey: "access_token") as? String
        expireDate = coder.decodeObject(forKey: "expireDate") as? Date
        uid = coder.decodeObject(forKey: "uid") as? String
        screen_name = coder.decodeObject(forKey: "screen_name") as? String
        avatar_large = coder.decodeObject(forKey: "avatar_large") as? String
    }
}

extension UserAccount: NSSecureCoding {
    //要實現 NSSecureCoding ，不然 NSKeyedArchiver.archivedData 永遠無法寫入
    static var supportsSecureCoding: Bool {
        return true
    }
}

//在 extension 中只允許寫便利構造函數，不能寫指定構造函數 required init?(coder: NSCoder)

