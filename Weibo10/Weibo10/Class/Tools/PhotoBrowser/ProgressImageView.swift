//
//  ProgressImageView.swift
//  Weibo10
//
//  Created by Derek on 2020/4/15.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

///帶進度的圖像視圖
///面試題：如果在 UIImageView 的 drawRect 中繪圖會怎樣
///ans: 不會執行 drawRect 函數
class ProgressImageView: UIImageView {
    
    ///外部傳遞的進度值 0-1
    var progress: CGFloat = 0 {
        didSet {
            progressView.progress = progress
        }
    }
    
    //MARK: - 構造函數
    //一旦給構造函數指定了參數，系統不再提供默認的構造函數
    init() {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        addSubview(progressView)
        progressView.backgroundColor = .clear
        progressView.snp_makeConstraints { (make) in
            make.edges.equalTo(self.snp_edges)
        }
    }
    
    //MARK: - 懶加載控件
    private lazy var progressView = ProgressView()
}

///進度視圖
private class ProgressView: UIView {
    
    ///內部使用的進度值 0-1
    var progress: CGFloat = 0 {
        didSet {
            //重繪視圖
            setNeedsDisplay()
        }
    }
    
    //rect == bounds
    override func draw(_ rect: CGRect) {
        
        let center = CGPoint(x: rect.width * 0.5, y: rect.height * 0.5)
        let r = min(rect.width, rect.height) * 0.5
        let start = CGFloat(-Double.pi / 2)
        let end = start + progress * 2 * CGFloat(Double.pi)
        
        /*
           arcCenter: 中心點
           radius: 半徑
           startAngle: 起始弧度
           endAngle: 截至弧度
           clockwise: 是否順時針
         */
        let path = UIBezierPath(arcCenter: center, radius: r, startAngle: start, endAngle: end, clockwise: true)
        
        //添加到中心點的連線
        path.addLine(to: center)
        path.close()
        UIColor(white: 1.0, alpha: 0.3).setFill()
        path.fill()
    }
}
