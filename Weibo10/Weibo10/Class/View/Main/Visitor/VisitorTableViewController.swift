//
//  VisitorTableViewController.swift
//  Weibo10
//
//  Created by Derek on 2020/3/29.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

class VisitorTableViewController: UITableViewController {

    //用戶登陸標記
    private var userLogon = UserAccountViewModel.sharedUserAccount.userLogon
    
    //訪客視圖
    /*
        1.應用程序中有幾個 visitorView? - 每個控制器各自有不同的訪客視圖 (可用 viewDidAppear 印出證明)
        2. 訪客視圖如果用懶加載會怎樣？ - 如果使用懶加載，訪客視圖始終都會被創建出來
           這邊的需求是如果登入成功就不秀出訪客視圖，但使用懶加載他會一直都在，造成內存消耗
     */
    var visitorView: VisitorView?
    
    override func loadView() {
        /*
         如果用戶已經登陸，調用super.loadView()
         如果用戶沒有登陸，不調用super.loadView()，並替換根視圖
         */
        userLogon ? super.loadView() : setupVisitorView()
    }
    
    ///設置訪客視圖 - 讓每一個小函數
    private func setupVisitorView() {
        //替換根視圖
        visitorView = VisitorView()
        view = visitorView
        
        //添加監聽方法
        visitorView?.registerButton.addTarget(self, action: #selector(visitorViewDidRegister), for: .touchUpInside)
        visitorView?.loginButton.addTarget(self, action: #selector(visitorViewDidLogin), for: .touchUpInside)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "註冊", style: .plain, target: self, action: #selector(visitorViewDidRegister))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登錄", style: .plain, target: self, action: #selector(visitorViewDidLogin))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //print(visitorView)
    }
}

//MARK: - 訪客視圖監聽方法
extension VisitorTableViewController {
    
    @objc func visitorViewDidRegister() {
        print("註冊")
    }
    @objc func visitorViewDidLogin() {
        print("登錄")
        let vc = OAuthViewController()
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        present(nav, animated: true, completion: nil)
    }
}
