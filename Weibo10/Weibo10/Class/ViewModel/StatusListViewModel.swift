//
//  StatusListViewModel.swift
//  Weibo10
//
//  Created by Derek on 2020/4/1.
//  Copyright © 2020 Derek. All rights reserved.
//

import Foundation
import SDWebImage

///微博數據列表模型 - 封裝網路方法
class StatusListViewModel {
    
    ///微博數據數組 - 上拉/下拉刷新
    lazy var statusList = [StatusViewModel]()
    
    ///加載網路數據
    func loadStatus(isPullup: Bool, finished:@escaping (_ isSuccess: Bool) -> ()) {
        
        //下拉刷新 - 數組中第一條微博的 id
        let since_id = isPullup ? 0 : (statusList.first?.status.id ?? 0)
        //上拉刷新 - 數組中最後一條微博的 id
        let max_id = isPullup ? (statusList.last?.status.id ?? 0) : 0
        
        NetworkTools.sharedTools.loadStatus(since_id: since_id, max_id: max_id) { (result, error) in
            if error != nil {
                print("出錯了")
                finished(false)
                return
            }
            
            guard let dict = result as? [String: Any] else {
                return
            }
            guard let array:[[String: Any]] = dict["statuses"] as? [[String: Any]] else {
                print("數據格式錯誤")
                finished(false)
                return
            }
            
            //遍歷字典的數組，字典轉模型
            //1. 可變數組
            var dataList = [StatusViewModel]()
            //2. 遍歷數組
            for dict in array {
                dataList.append(StatusViewModel(status: Status(dict: dict)))
            }
            
            print("刷新到 \(dataList.count)條數據")
            //3. 拼接數據
            //判斷是否是上拉刷新
            if max_id > 0 {
                self.statusList += dataList
            } else {
                self.statusList = dataList + self.statusList
            }
            
            //緩存單張圖片 - 目的是拿到單圖大小以便佈局
            self.cacheSingleImage(dataList: dataList, finished: finished)
        }
    }
    
    ///緩存單張圖片
    private func cacheSingleImage(dataList: [StatusViewModel], finished:@escaping (_ isSuccess: Bool) -> ()) {
        
        //1.創建調度組
        let group = DispatchGroup()
        
        //緩存數據長度
        var dataLength = 0
        
        //遍歷視圖模型數組
        for vm in dataList {
            //判斷圖片數量是否是單張圖片
            if vm.thumbnailUrls?.count != 1 {
                continue
            }
            //獲取 url
            let url = vm.thumbnailUrls![0]
            print("開始緩存圖像", url)
            //SDWebImage - 下載圖像(緩存是自動完成的)
            //入組 - 監聽後續的 block
            group.enter()
            //SDWebImage 的核心下載函數，如果本地緩存已經存在，同樣會通過完成回調返回
            //如果設置了 .retryFailed，如果下載失敗，block 會結束一次，會做一次出組
            //SDWebImage 會重新執行下載，下載完成後，再次調用 block 中的代碼
            //再次調用出組函數，造成調度組的不匹配，所以要刪除 .retryFailed
            //.refreshCached，第一次發起網路請求，會把緩存圖片的一個 hash 值發送給服務器
            //服務器比對 hash 值，如果和服務器的內容一致，服務器返回的狀態碼是 304，表示服務器內容沒有變化
            //如果不是 304，會再次發起網路請求，獲得更新後的內容
            SDWebImageManager.shared.loadImage(with: url, options: [], context: nil, progress: nil) { (image, _, _, _, _, _) in
                
                //單張圖片下載完成 - 計算長度
                guard let image = image else { return }
                let data = UIImage.pngData(image)
                //累加二進制數據長度
                dataLength += data()?.count ?? 0
                
                //出組
                group.leave()
            }
        }
        //監聽調度組完成
        group.notify(queue: .main) {
            print("緩存完成 \(dataLength / 1024)K")
            //完成回調 - 控制器才開始刷新表格
            finished(true)
        }
    }
}
