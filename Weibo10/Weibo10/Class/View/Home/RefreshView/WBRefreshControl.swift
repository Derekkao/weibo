//
//  WBRefreshControl.swift
//  Weibo10
//
//  Created by Derek on 2020/4/6.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

//下拉刷新控件偏移量
private let WBRefreshControlOffset: CGFloat = -60
///自定義刷新控件 - 負責處理刷新邏輯
class WBRefreshControl: UIRefreshControl {
    
    //MARK - KVO 監聽方法
    //1. 始終待在螢幕上
    //2. 下拉的時候，frame 的 Y 一直變小，相反（向上推）一直變大
    //3. 默認的 Y 是0
    //箭頭旋轉標記
    private var rotateFlag = false
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if frame.origin.y > 0 {
            return
        }
        //判斷臨界點
        if frame.origin.y < WBRefreshControlOffset && !rotateFlag {
            print("反過來")
            rotateFlag = true
            refreshView.rotateTipIcon()
        } else if frame.origin.y >= WBRefreshControlOffset && rotateFlag {
            print("轉過去")
            rotateFlag = false
            refreshView.rotateTipIcon()
        }
        print(frame)
    }
    
    override init() {
        super.init()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    private func setupUI() {
        addSubview(refreshView)
        //從 XIB 加載的控件需要指定大小約束
        refreshView.snp_makeConstraints { (make) in
            make.centerX.equalTo(self.snp_centerX)
            make.centerY.equalTo(self.snp_centerY)
            make.size.equalTo(refreshView.bounds.size)
        }
        
        //使用 KVO 監聽位置變化 - 主隊列，當主線程有任務，就不調度隊列中的任務執行
        //讓當前運行循環中所有代碼執行完畢後，運行循環結束前，開始監聽
        //方法觸發會在下一次運行循環開始
        DispatchQueue.main.async {
            self.addObserver(self, forKeyPath: "frame", options: [], context: nil)
        }
    }
    
    deinit {
        //刪除 KVO 監聽方法
        self.removeObserver(self, forKeyPath: "frame")
    }
    
    //MARK: - 懶加載控件
    private lazy var refreshView = WBRefreshView.refreshView()
    
}

///刷新視圖 - 負責處理動畫顯示
class WBRefreshView: UIView {
    
    @IBOutlet weak var tipIconView: UIImageView!
    
    //從 XIB 加載視圖
    class func refreshView() -> WBRefreshView {
        //推薦使用  UINib 的方式加載 XIB
        let nib = UINib(nibName: "WBRefreshView", bundle: nil)
        return nib.instantiate(withOwner: nil, options: [:])[0] as! WBRefreshView
    }
    
    ///旋轉圖標動畫
    func rotateTipIcon() {
        //旋轉動畫特點：順時針優先 + 就近原則
        UIView.animate(withDuration: 0.5) {
            self.tipIconView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        }
    }
}
