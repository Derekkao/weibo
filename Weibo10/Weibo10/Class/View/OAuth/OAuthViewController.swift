//
//  OAuthViewController.swift
//  Weibo10
//
//  Created by Derek on 2020/3/30.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import Toast_Swift

///用戶登入控制器
class OAuthViewController: UIViewController {

    private lazy var webView = UIWebView(frame: UIScreen.main.bounds)
    
    //MARK: - 監聽方法
    @objc private func close() {
        view.activityStopAnimating()
        dismiss(animated: true, completion: nil)
    }
    
    ///自動填充用戶名和密碼 - web 注入 (以代碼的方式向 web 頁面添加內容)
    @objc private func autoFill() {
        let js = "document.getElementById('userId').value = '00886900625360';" + "document.getElementById('passwd').value = 'qxzQ*:yicC:fV9K';"
        
        //讓 webView 執行 js
        webView.stringByEvaluatingJavaScript(from: js)
    }
    
    override func loadView() {
        view = webView
        navigationItem.title = "登錄新浪微博"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "關閉", style: .plain, target: self, action: #selector(close))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "自動填充", style: .plain, target: self, action: #selector(autoFill))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //設置代理
        webView.delegate = self

        //在開發中，如果用純代碼開發，視圖最好指定背景顏色，如果為nil，會影響渲染效率
        view.backgroundColor = .white
        
        //加載頁面
        self.webView.loadRequest(URLRequest(url: NetworkTools.sharedTools.oauthURL))
    }
}

//MARK: UIWebViewDelegate
extension OAuthViewController: UIWebViewDelegate {
    ///如果 iOS 代理方法中有返回 bool，通常返回 true 很正常，返回 false 不能正常工作
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        print(request)
        print("host: ", request.url?.host ?? "")
        print("query: ", request.url?.query ?? "")
        
        //目標：如果是百度，就不加載 (一進去第一次不會是百度，所以找到百度之後就不加載)
        // 1. 判斷訪問的主機是否是 www.baidu.com
        guard let url = request.url, url.host == "www.baidu.com" else {
            //不是百度的話，繼續加載
            return true
        }

        // 2. 從百度地址的 url 中提取 code= 是否存在
        guard let query = url.query, query.hasPrefix("code=") else {
            print("取消授權")
            self.close()
            return false
        }

        // 3. 從 query 字串中提取 code= 後面的授權碼
        let code = query.substring(from: "code=".endIndex)
        print(query)
        print("授權碼是 " + code)
        
        // 4. 加載 accessToken
        UserAccountViewModel.sharedUserAccount.loadAccessToken(code: code) { (isSuccessed) in
            if !isSuccessed {
                print("失敗了")
                self.view.makeToast("無法取得授權！")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.close()
                }
                return
            }
            print("成功了")
            self.dismiss(animated: false) {
                //通知中心是同步的，一旦發送通知，會先執行監聽方法，結束後，才執行後續代碼
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: WBSwitchRootViewControllerNotification), object: "welcome")
            }
        }
        return false
    }
}

extension OAuthViewController {
    func webViewDidStartLoad(_ webView: UIWebView) {
        view.activityStartAnimating(activityColor: .gray, backgroundColor: .white)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        view.activityStopAnimating()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("錯誤在這：", error.localizedDescription)
        if error.localizedDescription != "Frame load interrupted" {
            DispatchQueue.main.async {
                self.view.makeToast("您的網路不給力！")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.close()
                }
            }
        }
    }
}
