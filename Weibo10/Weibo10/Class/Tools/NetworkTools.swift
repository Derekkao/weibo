//
//  NetworkTools.swift
//  Weibo10
//
//  Created by Derek on 2020/3/30.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import AFNetworking

enum HMRequestMethod: String {
    case GET = "GET"
    case POST = "POST"
}

//MARK: 網路工具
class NetworkTools: AFHTTPSessionManager {
    
    //MARK: - 應用程序信息
    private let appKey = "1204201428"
    private let appSecret = "d77aa290f3ecac95228795810a104040"
    private let redirectUrl = "http://www.baidu.com"
    
    typealias HMRequestCallBack = (_ result:Any?, _ error: Error?) -> ()
    
    //單例
    static let sharedTools: NetworkTools = {
        let tools = NetworkTools(baseURL: nil)
        //設置反序列化數據格式 - 系統會自動將 OC 框架中的 NSSet 轉換成 Set
        tools.responseSerializer.acceptableContentTypes?.insert("text/html")
        return tools
    }()
}

//MARK: - 發布微博
extension NetworkTools {
    
    /// - see: [https://open.weibo.com/wiki/2/statuses/share](https://open.weibo.com/wiki/2/statuses/share)
    func sendStatus(status: String, image: UIImage?, finished:@escaping HMRequestCallBack) {
        
        var params = [String: Any]()
        
        //2. 設置參數
        params["status"] = status
        
        //3. 判斷是否上傳圖片
        //注意，這邊網路教學的 urlString 上傳文字和上傳圖片是分開的，所以才需要判斷
        if image == nil {
            let urlString = "https://api.weibo.com/2/statuses/share.json"
            tokenRequest(method: .POST, urlString: urlString, parameters: params, finished: finished)
        } else {
            let urlString = "https://api.weibo.com/2/statuses/share.json"
            if let data = image?.pngData() {
                upload(urlString: urlString, data: data, name: "pic", parameters: params, finished: finished)
            }
        }
    }
}

//MARK: - 微博數據相關方法
extension NetworkTools {
    ///加載微博數據
    /// - see: [https://open.weibo.com/wiki/2/statuses/home_timeline](https://open.weibo.com/wiki/2/statuses/home_timeline)
    ///since_id: 若指定此参数，则返回ID比since_id大的微博（即比since_id时间晚的微博），默认为0。
    ///max_id: 若指定此参数，则返回ID小于或等于max_id的微博，默认为0。
    func loadStatus(since_id: Int, max_id: Int, finished:@escaping HMRequestCallBack) {
        
        var params = [String: Any]()
        
        //判斷是否下拉
        if since_id > 0 {  //代表新的文章
            params["since_id"] = since_id
        } else if max_id > 0 {  //上拉參數
            params["max_id"] = max_id - 1
        }
        
        //2. 準備網路參數
        let urlString = "https://api.weibo.com/2/statuses/home_timeline.json"
        
        //3. 發起網路請求
        tokenRequest(method: .GET, urlString: urlString, parameters: params, finished: finished)
    }
}

//MARK: - 用戶相關方法
extension NetworkTools {
    func loadUserInfo(uid: String, finished:@escaping  HMRequestCallBack) {
        
        var params = [String: Any]()
        
        //2. 處理網路參數
        let urlString = "https://api.weibo.com/2/users/show.json"
        params["uid"] = uid
        
        tokenRequest(method: .GET, urlString: urlString, parameters: params, finished: finished)
    }
}

//MARK: - OAuth 相關方法
extension NetworkTools {
    //計算型屬性可以寫在 extension
    ///OAuth 授權 URL
    /// - see: [https://open.weibo.com/wiki/Oauth2/authorize](https://open.weibo.com/wiki/Oauth2/authorize)
    var oauthURL: URL {
        let urlString = "https://api.weibo.com/oauth2/authorize?client_id=\(appKey)&redirect_uri=\(redirectUrl)"
        return URL(string: urlString)!
    }
    
    /// - see: [https://open.weibo.com/wiki/Oauth2/access_token](https://open.weibo.com/wiki/Oauth2/access_token)
    func loadAccessToken(code: String, finished:@escaping HMRequestCallBack) {
        let urlString = "https://api.weibo.com/oauth2/access_token"
        let params = [
            "client_id": appKey,
            "client_secret": appSecret,
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": redirectUrl
        ]
        request(method: .POST, urlString: urlString, parameters: params, finished: finished)
        
        /*
        //測試返回的數據內容 - AFN 默認的響應格式是 JSON，會直接反序列化
        // 1. 設置相應數據格式是二進制的
        responseSerializer = AFHTTPResponseSerializer()
        
        // 2. 發起網路請求
        post(urlString, parameters: params, progress: nil, success: { (_, result) in
            
            //將二進制數據轉換成字串
            let json = String(data: result as! Data, encoding: .utf8)
            print(json)
            
            /* <以下網路範例，僅供參考>
            let json = String(data: result as! Data, encoding: .utf8)
            let data = json?.data(using: .utf8)
            do {
                let dic = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                print(dic)
            }
            catch let e as NSError {
                print(e.localizedDescription)
            }
            */
        }) { (_, err) in
            print(err)
        }
        */
    }
}

//MARK: - 封裝 AFN 網路方法
extension NetworkTools {
    
    ///追加 token 方法
    ///不是按照課程的寫法，自己寫出來
    private func appendToken(parameters: [String: Any]?) -> [String: Any]? {
        guard let token = UserAccountViewModel.sharedUserAccount.accessToken else {
            //如果字典為 nil，通知調用方，token 無效
            print("token 出錯了")
            return nil
        }
        //設置 parameters 字典
        //判斷參數字典是否有值
        var para: [String: Any]? = parameters
        
        if para == nil {
            para = [String: Any]()
        }
        para!["access_token"] = token

        return para
    }
    
    ///使用 token 進行網路請求
    private func tokenRequest(method:HMRequestMethod, urlString: String, parameters: [String: Any]?, finished:@escaping HMRequestCallBack) {
        
        //除了原本的 token，加入其他的 parameters
        let para = appendToken(parameters: parameters)
        
        //2. 發起網路請求
        request(method: method, urlString: urlString, parameters: para, finished: finished)
    }
    
    ///網路請求
    private func request(method:HMRequestMethod, urlString: String, parameters: [String: Any]?, finished:@escaping HMRequestCallBack) {
        
        if method == .GET {
            self.get(urlString, parameters: parameters, progress: nil, success: { (_, result) in
                finished(result, nil)
            }) { (_, err) in
                //在開發網路應用時，錯誤不要提示給用戶，但是要輸出
                print(err)
                finished(nil, err)
            }
        } else {
            self.post(urlString, parameters: parameters, progress: nil, success: { (_, result) in
                finished(result, nil)
            }) { (_, err) in
                print(err)
                finished(nil, err)
            }
        }
    }
    
    ///上傳文件
    private func upload(urlString: String, data: Data, name: String, parameters: [String: Any]?, finished:@escaping HMRequestCallBack) {
        
        //除了原本的 token，加入其他的 parameters
        let para = appendToken(parameters: parameters)
        
        post(urlString, parameters: para, constructingBodyWith: { (formData) in
            /*
               1. data 要上傳文件的二進制數據
               2. name 是服務器定義的字段名稱 - 後台接口文檔會提示
               3. fileName 是保存在服務器的文件名，但是：現在通常可以亂寫，後台會做後續的處理
                  - 根據上傳的文件，生成縮略圖、中等圖、高清圖
                  - 保存在不同路徑，並且自動生成文件名
                  - fileName 是 HTTP 協議定義的屬性
               4. mimeType / contentType: 客戶端告訴服務器二進制數據的準確類型
                  - 大類型/小類型  *image/jpg  image/gif  image/png
                  - 如果不想告訴服務器準確的類型，可以用 *application/octet-stream
             */
            formData.appendPart(withFileData: data, name: name, fileName: "xxx", mimeType: "application/octet-stream")
        }, success: { (_, result) in
            finished(result, nil)
        }) { (_, err) in
            print(err)
            finished(nil, err)
        }
    }
}
