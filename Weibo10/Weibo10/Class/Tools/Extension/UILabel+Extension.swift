//
//  UILabel+Extension.swift
//  Weibo10
//
//  Created by Derek on 2020/3/30.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

extension UILabel {
    
    ///便利構造函數
    convenience init(title: String, fontSize: CGFloat = 14, color: UIColor = .darkGray, screenInset: CGFloat = 0 //相對於屏幕左右的縮緊，默認為0，居中顯示，如果設置，則左對齊
    ) {
        self.init()
        
        text = title
        textColor = color
        font = .systemFont(ofSize: fontSize)
        numberOfLines = 0
        
        if screenInset == 0 {
            textAlignment = .center
        } else {
            //設置換行寬度
            preferredMaxLayoutWidth = UIScreen.main.bounds.width - 2 * screenInset
            textAlignment = .left
        }
        sizeToFit()
    }
}
