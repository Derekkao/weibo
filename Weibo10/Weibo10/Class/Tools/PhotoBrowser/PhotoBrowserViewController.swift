//
//  PhotoBrowserViewController.swift
//  Weibo10
//
//  Created by Derek on 2020/4/14.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

private let PhotoBrowserViewCellId = "PhotoBrowserViewCellId"

///照片瀏覽器
class PhotoBrowserViewController: UIViewController {

    ///照片 URL 數組
    private var urls: [URL]
    ///當前選中的照片索引
    private var currentIndexPath: IndexPath
    
    //MARK: - 監聽方法
    @objc private func close() {
        dismiss(animated: true, completion: nil)
    }
    
    ///保存照片
    @objc private func save() {
        let cell = collectionView.visibleCells[0] as! PhotoBrowserCell
        guard let image = cell.imageView.image else { return }
        
        //保存圖片
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        let message = error == nil ? "保存成功" : "保存失敗"
        view.makeToast(message)
    }
    
    //MARK: - 構造函數
    init(urls: [URL], indexPath: IndexPath) {
        self.urls = urls
        self.currentIndexPath = indexPath
        
        //調用父類方法
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //和 xib & sb 等價的，主要職責創建視圖層次結構，loadView 函數執行完畢，view 上的元素要全部創建完成
    //如果 view == nil，系統會在調用 view 的 getter 方法時，自動調用 loadView，創建 view
    override func loadView() {
        var rect = UIScreen.main.bounds
        rect.size.width += 20
        view = UIView(frame: rect)
        
        setupUI()
    }
    
    //是視圖加載完成被調用，loadView 執行完畢被執行
    //主要做數據加載，或者其他處理
    //但是目前市場上很多程序，沒有實現 loadView，所有建立子控件的代碼都在 viewDidLoad 中
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //讓 collectionView 滾動到指定位置
        collectionView.scrollToItem(at: currentIndexPath, at: .centeredHorizontally, animated: false)
    }
    
    //MARK: - 懶加載控件
    private lazy var collectionView: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: PhotoBrowserViewLayout())
    private lazy var closeButton = UIButton(title: "關閉", color: .white, bgColor: .darkGray, fontSize: 14)
    private lazy var saveButton = UIButton(title: "保存", color: .white, bgColor: .darkGray, fontSize: 14)
    
    private class PhotoBrowserViewLayout: UICollectionViewFlowLayout {
        override func prepare() {
            super.prepare()
            
            itemSize = collectionView!.bounds.size
            minimumLineSpacing = 0
            minimumInteritemSpacing = 0
            scrollDirection = .horizontal
            collectionView?.isPagingEnabled = true
            collectionView?.bounces = false
            collectionView?.showsHorizontalScrollIndicator = false
        }
    }
}

//MARK: - 設置介面
private extension PhotoBrowserViewController {
    func setupUI() {
        view.addSubview(collectionView)
        view.addSubview(closeButton)
        view.addSubview(saveButton)
        
        collectionView.frame = view.bounds
        closeButton.snp_makeConstraints { (make) in
            make.bottom.equalTo(view.snp_bottom).offset(-8)
            make.left.equalTo(view.snp_left).offset(8)
            make.size.equalTo(CGSize(width: 100, height: 36))
        }
        saveButton.snp_makeConstraints { (make) in
            make.bottom.equalTo(view.snp_bottom).offset(-8)
            make.right.equalTo(view.snp_right).offset(-28)
            make.size.equalTo(CGSize(width: 100, height: 36))
        }
        
        closeButton.addTarget(self, action: #selector(close), for: .touchUpInside)
        saveButton.addTarget(self, action: #selector(save), for: .touchUpInside)
        
        prepareCollectionView()
    }
    
    func prepareCollectionView() {
        collectionView.register(PhotoBrowserCell.self, forCellWithReuseIdentifier: PhotoBrowserViewCellId)
        collectionView.dataSource = self
    }
}

//MARK: - UICollectionViewDataSource
extension PhotoBrowserViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return urls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoBrowserViewCellId, for: indexPath) as! PhotoBrowserCell
        cell.backgroundColor = .black
        cell.imageURL = urls[indexPath.item]
        cell.photoDelegate = self
        return cell
    }
}

//MARK: - PhotoBrowserCellDelegate
extension PhotoBrowserViewController: PhotoBrowserCellDelegate {
    
    func photoBrowserCellDidTapImage() {
        close()
    }
}

//MARK: - 解除轉場動畫協議
extension PhotoBrowserViewController: PhotoBrowserDismissDelegate {
    
    func imageViewForDismiss() -> UIImageView {
        let iv = UIImageView()
        
        //設置填充模式
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        
        //設置圖像 - 直接從當前顯示的 cell 中獲取
        let cell = collectionView.visibleCells[0] as! PhotoBrowserCell
        iv.image = cell.imageView.image
        
        //設置位置 - 座標轉換（由父視圖進行轉換）
        iv.frame = cell.scrollView.convert(cell.imageView.frame, to: UIApplication.shared.keyWindow)
        
        return iv
    }
    
    func indexPathForDismiss() -> IndexPath {
        return collectionView.indexPathsForVisibleItems[0]
    }
}
