//
//  MessageTableViewController.swift
//  Weibo10
//
//  Created by Derek on 2020/3/28.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

class MessageTableViewController: VisitorTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        visitorView?.setupInfo(imageName: "chat", title: "登錄後，別人評論你的微博，發給你的訊息，都會在這裡收到通知")
    }
}
