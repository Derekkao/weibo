//
//  WelcomeViewController.swift
//  Weibo10
//
//  Created by Derek on 2020/4/1.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import SDWebImage

class WelcomeViewController: UIViewController {

    //設置界面，視圖的層次結構
    override func loadView() {
        super.loadView()
        setupUI()
    }
    
    //視圖加載完後的後續處理，通常用來設置數據
    override func viewDidLoad() {
        super.viewDidLoad()
        
        iconView.sd_setImage(with: UserAccountViewModel.sharedUserAccount.avatarUrl, placeholderImage: nil, options: [])
    }
    
    //視圖已經顯示，通常可以顯示動畫/鍵盤鍵盤處理
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //1. 更改約束 -> 改變位置
        iconView.snp_updateConstraints { (make) in
            make.bottom.equalTo(view.snp_bottom).offset(-view.bounds.height + 200)
        }
        welcomeLabel.alpha = 0
        UIView.animate(withDuration: 1.2, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 10, options: [], animations: {
            
            //自動佈局的動畫
            self.view.layoutIfNeeded()
        }) { (_) in
            UIView.animate(withDuration: 0.8, animations: {
                self.welcomeLabel.alpha = 1
            }) { (_) in
                print("OK")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: WBSwitchRootViewControllerNotification), object: nil)
            }
        }
    }
    
    //MARK: - 懶加載控件
    private lazy var iconView: UIImageView = {
        //let iv = UIImageView(image: #imageLiteral(resourceName: "user"))
        let iv = UIImageView()
        iv.layer.cornerRadius = 50
        iv.clipsToBounds = true
        return iv
    }()
    private lazy var welcomeLabel = UILabel(title: "歡迎歸來", fontSize: 18, color: .darkGray)
}

//MARK: - 設置界面
extension WelcomeViewController {
    private func setupUI() {
        view.addSubview(iconView)
        view.addSubview(welcomeLabel)
        iconView.snp_makeConstraints { (make) in
            make.centerX.equalTo(view.snp_centerX)
            make.bottom.equalTo(view.snp_bottom).offset(-200)
            make.width.equalTo(100)
            make.height.equalTo(100)
        }
        welcomeLabel.snp_makeConstraints { (make) in
            make.centerX.equalTo(iconView.snp_centerX)
            make.top.equalTo(iconView.snp_bottom).offset(16)
        }
    }
}
