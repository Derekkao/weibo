//
//  StatusRetweetedCell.swift
//  Weibo10
//
//  Created by Derek on 2020/4/3.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

///轉發微博的 cell
class StatusRetweetedCell: StatusCell {
    
    ///微博視圖模型
    ///如果繼承父類的屬性
    ///1. 需要 override
    ///2. 不需要 super
    ///3. 先執行父類的 didSet，再執行子類的 didSet -> 只關心子類相關設置就夠了
    override var viewModel: StatusViewModel? {
        didSet {
            retweetedLabel.text = viewModel?.retweetedText
            
            pictureView.snp_updateConstraints { (make) in
                
                //根據配圖數量，決定配圖視圖的頂部間距
                let offset = viewModel?.thumbnailUrls?.count ?? 0 > 0 ? StatusCellMargin : 0
                make.top.equalTo(retweetedLabel.snp_bottom).offset(offset)
            }
        }
    }
    
    //MARK: - 懶加載控件
    ///背景按鈕
    private lazy var backButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        return button
    }()
    ///轉發標籤
    private lazy var retweetedLabel = UILabel(title: "轉發微博", fontSize: 14, color: .darkGray, screenInset: StatusCellMargin)
}

// MARK: - 設置介面
extension StatusRetweetedCell {
    override func setupUI() {
        super.setupUI()
        //1. 添加控件
        contentView.insertSubview(backButton, belowSubview: pictureView)
        contentView.insertSubview(retweetedLabel, aboveSubview: backButton)
        //2. 自動佈局
        backButton.snp_makeConstraints { (make) in
            make.top.equalTo(contentLabel.snp_bottom).offset(StatusCellMargin)
            make.left.equalTo(contentView.snp_left)
            make.right.equalTo(contentView.snp_right)
            make.bottom.equalTo(bottomView.snp_top)
        }
        retweetedLabel.snp_makeConstraints { (make) in
            make.top.equalTo(backButton.snp_top).offset(StatusCellMargin)
            make.left.equalTo(backButton.snp_left).offset(StatusCellMargin)
        }
        pictureView.snp_makeConstraints { (make) in
            make.top.equalTo(retweetedLabel.snp_bottom).offset(StatusCellMargin)
            make.left.equalTo(retweetedLabel.snp_left)
            make.width.equalTo(300)
            make.height.equalTo(90)
        }
    }
}
