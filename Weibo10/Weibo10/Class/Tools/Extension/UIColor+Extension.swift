//
//  UIColor+Extension.swift
//  Weibo10
//
//  Created by Derek on 2020/4/14.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

extension UIColor {
    class func randomColor() -> UIColor {
        let r = CGFloat(arc4random() % 256) / 255
        let g = CGFloat(arc4random() % 256) / 255
        let b = CGFloat(arc4random() % 256) / 255
        return UIColor(red: r, green: g, blue: b, alpha: 1)
    }
}
