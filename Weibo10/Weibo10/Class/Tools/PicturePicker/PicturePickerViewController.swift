//
//  PicturePickerViewController.swift
//  PhootSelection
//
//  Created by Derek on 2020/4/10.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

///可重用 cell
private let PicturePickerCellId = "PicturePickerCellId"
///最大選擇照片數量
private let PicturePickerMaxCount = 8

///照片選擇控制器
class PicturePickerViewController: UICollectionViewController {

    ///配圖數組
    lazy var pictures = [UIImage]()
    
    ///當前用戶選到的照片索引
    private var selectedIndex = 0
    
    init() {
        super.init(collectionViewLayout: PicturePickerLayout())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.backgroundColor = UIColor(white: 0.9, alpha: 1.0)

        //註冊可重用 cell
        self.collectionView!.register(PicturePickerCell.self, forCellWithReuseIdentifier: PicturePickerCellId)
    }
    
    //MARK: - 照片選擇器佈局
    private class PicturePickerLayout: UICollectionViewFlowLayout {
        override func prepare() {
            super.prepare()
            
            // iOS 9.0 之後，尤其是 iPad 支持分屏，不建議過分依賴 UIScreen 作為佈局參照
            // iphone 6s - 2/ iphone 6s+ 3
            let count: CGFloat = 4
            let margin = UIScreen.main.scale * 4
            let w = ((collectionView?.bounds.width)! - (count + 1) * margin) / count
            itemSize = CGSize(width: w, height: w)
            sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: 0, right: margin)
            minimumLineSpacing = margin
            minimumInteritemSpacing = margin
        }
    }
}

extension PicturePickerViewController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //保證末尾有一個加號按鈕，滿八張就不顯示加號按鈕了
        return pictures.count + (pictures.count == PicturePickerMaxCount ? 0 : 1)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PicturePickerCellId, for: indexPath) as! PicturePickerCell
        cell.pictureDelegate = self
        //如果沒有超限的話 (indexPath.item < pictures.count)，設圖給他
        cell.image = (indexPath.item < pictures.count) ? pictures[indexPath.item] : nil
        
        return cell
    }
}

extension PicturePickerViewController: PicturePickerCellDelegate {
    
    fileprivate func picturePickerCellDidAdd(cell: PicturePickerCell) {
        //判斷是否允許訪問相冊
        /*
            photoLibrary       保存的照片(可以刪除) + 同步的照片(不允許刪除)
            savedPhotosAlbum   保存的照片/屏幕截圖/拍照
         */
        if !UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            print("無法訪問照片庫")
            return
        }
        
        //紀錄當前用戶選中的照片索引
        selectedIndex = collectionView.indexPath(for: cell)!.item
        
        let picker = UIImagePickerController()
        
        //設置代理
        picker.delegate = self
        //適合用於頭像選擇
        //picker.allowsEditing = true
        
        picker.modalPresentationStyle = .fullScreen
        present(picker, animated: true, completion: nil)
    }
    fileprivate func picturePickerCellDidRemove(cell: PicturePickerCell) {
        
        //1. 獲取照片索引
        let indexPath = collectionView.indexPath(for: cell)
        
        //2. 判斷索引是否超出上限
        if indexPath!.item >= pictures.count {
            return
        }
        
        //3. 刪除數據
        pictures.remove(at: indexPath!.item)
        
        //4. 動畫刷新視圖 - 會重新調用數據行數的數據源方法(內部實現要求 3 - 刪除一個 indexPath - 數據源方法如果不返回 2，就會崩潰)
        //collectionView.deleteItems(at: [indexPath!])
        //reloadData 方法只是單純刷新數據，沒有動畫，但是不會檢測具體的 item 數量
        collectionView.reloadData()
    }
}

extension PicturePickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    ///照片選擇完成
    /*
        picker: 照片選擇控制器
        info: info 字典
        如果使用 cocos2dx 開發一個空白的模板遊戲，內存佔用 70MB
                iOS UI 的空白應用程序，大概 19MB
        一般應用程序，內存在 100MB 左右都是可以接受的，再高就需要注意
     */
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(info)
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        let scaleImage = image.scaleToWith(width: 600)
        
        //將圖像加到數組
        //判斷當前選中的索引是否超出數組上限
        if selectedIndex >= pictures.count {
            pictures.append(scaleImage)
        } else {
            //直接替換原先的照片，不會新加
            pictures[selectedIndex] = scaleImage
        }
        
        //刷新視圖
        collectionView.reloadData()
        
        //釋放控制器
        dismiss(animated: true, completion: nil)
    }
}

///PicturePickerCellDelegate 代理
///如果協議中包含 optional 的函數，協議需要使用 @objc 修飾
@objc
private protocol PicturePickerCellDelegate {
    
    ///添加照片
    @objc optional func picturePickerCellDidAdd(cell: PicturePickerCell)
    ///刪除照片
    @objc optional func picturePickerCellDidRemove(cell: PicturePickerCell)
}

///照片選擇 cell
private class PicturePickerCell: UICollectionViewCell {
    
    ///照片選擇代理
    weak var pictureDelegate: PicturePickerCellDelegate?
    
    var image: UIImage? {
        didSet {
            addButton.setImage(image ?? UIImage(named: "plus-1"), for: .normal)
            
            //隱藏刪除按鈕
            removeButton.isHidden = image == nil
        }
    }
    
    //MARK: - 監聽方法
    @objc func addPicture() {
        pictureDelegate?.picturePickerCellDidAdd?(cell: self)
    }
    
    @objc func removePicture() {
        pictureDelegate?.picturePickerCellDidRemove?(cell: self)
    }
    
    //MARK: - 構造函數
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    ///設置控件
    private func setupUI() {
        contentView.addSubview(addButton)
        contentView.addSubview(removeButton)
        
        addButton.frame = bounds
        removeButton.snp_makeConstraints { (make) in
            make.top.equalTo(contentView.snp_top)
            make.right.equalTo(contentView.snp_right)
            make.height.equalTo(20)
            make.width.equalTo(20)
        }
        
        addButton.addTarget(self, action: #selector(addPicture), for: .touchUpInside)
        removeButton.addTarget(self, action: #selector(removePicture), for: .touchUpInside)
        
        //設置填充模式
        addButton.imageView?.contentMode = .scaleAspectFill
    }
    
    //MARK: - 懶加載控件
    private lazy var addButton = UIButton(imageName: "plus-1")
    private lazy var removeButton = UIButton(imageName: "close")
}


